# Vanilla Minecraft
This is a Vanilla Minecraft server where i've set up daily commits as a form of
"backups".

# Running
With docker-compose:
```
docker-compose up
```

## With Docker Swarm
```
docker stack deploy --compose-file=docker-compose.yml
```

# Setting up a Cron
Here's my current cron:
```cron
@daily /home/minecraft/server/daily-backup.sh >> /var/log/minecraft/backup.log
```